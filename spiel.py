# -*- coding: utf-8 -*-


"""
Spielobjekte / -logik
---------------------
Hier stehen alle Objekte, die fuer einen Spieldurchlauf aus spielelogischer
Sicht gebraucht werden.
"""


import random


# # # # # # # # # # # # #
# Konstanten
# # # # # # # # # # # # #

SPIELFELD_GROESSE = 8


# # # # # # # # # # # # #
# Spielfeld
# # # # # # # # # # # # #

class Spielfeld():

    # Erzeuge ein quadratisches Spielfeld mit Groesse groesse und startwert an
    # jeder Stelle im Koordinatensystem
    def __init__(self, groesse, startwert, besitzer):

        # Besitzer des Spielfelds
        self.besitzer = besitzer

        # Zuletzt getroffene Koordinaten
        self.letzter_treffer = (0, 0)

        # Erzeuge Koordinatenkoordinaten mit Null als Anfangswert an jeder Stelle
        self.koordinaten = [
            [int(startwert) for x in range(groesse)] for y in range(groesse)
        ]
        

    # Darstellung fuer Entwicklungszwecke
    def __repr__(self):

        return "<{} von {}>\n{}".format(
            self.__class__.__name__,
            self.besitzer,
            "\n".join(
                (" ".join(
                    str(spalte) for spalte in zeile)
                ) for zeile in self.koordinaten
            )
        )


    # Positioniere ein Schiff auf dem Spielfeld
    # TODO: bitte noch zwei weitere Richtungen (links und oben) 2 = links
    def setze_schiff(self, schiff, x, y, richtung):
        # Richtung
        # 0 schiff von links nach rechts
        # 1 schiff von rechts nach links
        # 2 schiff von oben nach unten
        # 3 schiff von unten nach oben

        # Merke Laenge des Schiffes
        laenge = schiff.laenge

        # Weil die Koordinatenmatrixkoordinaten in y-Richtung um 1 nach oben
        # verschoben werden muessen, berechnen wir y_k und nutzen das, um
        # y-Koordinaten in der Koordinatenmatrix zu setzen ...
        # ... find ich aber meh. Kann man das nicht besser im Interface loesen?
        # Oder lieber das Spielfeld zu einem Dict machen, das von 1 bis 8 geht?
        y_k = y - 1

        # links nach rechts
        if (richtung == 0):
            # Brich ab, wenn Schiff ueber Spielfeldgrenz hinaus geht
            if ((x + laenge) > SPIELFELD_GROESSE):
                return False

            # Pruefe auf Ueberschneidung mit anderem Schiff und setze
            # Koordinaten im Spielfeld dort auf 2, wo das Schiff liegt
            for i in range(x, x+laenge):
                if self.koordinaten[y_k][i] == 2:
                    return False

                else:
                    self.koordinaten[y_k][i] = 2                

            # Speichere sonstige Daten ueber das Schiff
            schiff.gesetzt = True
            schiff.x = x
            schiff.y = y
            schiff.richtung = richtung

            return True

        # rechts nach links
        elif (richtung == 1):
            # Brich ab, wenn Schiff ueber Spielfeldgrenz hinaus geht
            if ((x - laenge) < (-1)):
                return False

            # Pruefe auf Ueberschneidung mit anderem Schiff und setze
            # Koordinaten im Spielfeld dort auf 2, wo das Schiff liegt
            for i in range(x, x-laenge, -1):
                if self.koordinaten[y_k][i] == 2:
                    return False

                else:
                    self.koordinaten[y_k][i] = 2

            # Speichere sonstige Daten ueber das Schiff
            schiff.gesetzt = True
            schiff.x = x
            schiff.y = y
            schiff.richtung = richtung

            return True

        # oben nach unten 
        elif (richtung == 2):
            # Brich ab, wenn Schiff ueber Spielfeldgrenz hinaus geht
            if ((y_k + laenge) > SPIELFELD_GROESSE):
                return False

            # Pruefe auf Ueberschneidung mit anderem Schiff und setze
            # Koordinaten im Spielfeld dort auf 2, wo das Schiff liegt
            for i in range(y_k, y_k+laenge):
                if self.koordinaten[i][x] == 2:
                    return False

                else:
                    self.koordinaten[i][x] = 2

            # Speichere sonstige Daten ueber das Schiff
            schiff.gesetzt = True
            schiff.x = x
            schiff.y = y
            schiff.richtung = richtung

            return True

        # unten nach oben
        elif (richtung == 3):
            # Brich ab, wenn Schiff ueber Spielfeldgrenz hinaus geht
            if ((y_k - laenge) < (-1)):
                return False

            # Pruefe auf Ueberschneidung mit anderem Schiff und setze
            # Koordinaten im Spielfeld dort auf 2, wo das Schiff liegt
            for i in range(y_k, y_k-laenge, -1):
                if self.koordinaten[i][x] == 2:
                    return False

                else:
                    self.koordinaten[i][x] = 2

            # Speichere sonstige Daten ueber das Schiff
            schiff.gesetzt = True
            schiff.x = x
            schiff.y = y
            schiff.richtung = richtung
            
            return True


# # # # # # # # # # # # #
# Schiff
# # # # # # # # # # # # #


# Erstmal für Init-Phase nutzen, danach ist nur noch Spielfeld interessant!
class Schiff():

    # Ereuge ein Schiff mit der Laenge laenge
    def __init__(self, laenge):

        self.gesetzt = False
        self.x = 0
        self.y = 0
        self.richtung = 0

        # Verhindere Erstellen zu kleiner oder zu grosser Schiffe
        if (0 < laenge < 5):
            # Laenge des Schiffs setzen
            self.laenge = laenge

        else:
            raise Exception("Ungueltige Schiffsgroesse")


    # Darstellung fuer Entwicklungszwecke
    def __repr__(self):

        return "<{}, Laenge: {}".format(
            self.__class__.__name__,
            self.laenge
        )


# # # # # # # # # # # # #
# Spieler (Mensch)
# # # # # # # # # # # # #


class Spieler():

    # Erzeuge einen neuen Spieler
    def __init__(self):

        # Schiffe für Init-Phase...erstmal ein Vorschlag ;)
        self.schiffe = [
            Schiff(4),
            Schiff(3),
            Schiff(3),
            Schiff(2),
            Schiff(2),
            Schiff(1),
            Schiff(1),
            Schiff(1)
        ]

        # Anzahl der Leben des Spielers, berechnet aus Gesamtlaenge aller
        # verfuegbaren Schiffe des Spielers; spaeter im Spiel wird dieser
        # Wert aus der Anzahl von 2en im eigenen Spielfeld berechnet,
        # Berechnung gilt also nur zu Beginn!
        self.leben = sum([schiff.laenge for schiff in self.schiffe])

        # Eigenes Spielfeld
        self.spielfeld_selbst = Spielfeld(SPIELFELD_GROESSE, 1, "Spieler")

        # Gegnerisches Spielfeld
        self.spielfeld_gegner = Spielfeld(SPIELFELD_GROESSE, 0, "Gegner")
        

    # Darstellung fuer Entwicklungszwecke
    def __repr__(self):

        return "<{}>\nLeben: {}\n{}\n{}".format(
            self.__class__.__name__,
            self.leben,
            self.spielfeld_selbst,
            self.spielfeld_gegner
        )


    def angriff(self, x, y, gegner):

        if ((self.spielfeld_gegner.koordinaten[y][x] == 3) or
                (self.spielfeld_gegner.koordinaten[y][x] == 1)):
            return None
        
        # Ausgewaehlte Koordinate ist Schiff
        if gegner.spielfeld_selbst.koordinaten[y][x] == 2:
            # Setze Position im eigenen Spielfeld des Gegners auf "getroffen"
            gegner.spielfeld_selbst.koordinaten[y][x] = 3

            # Setze die Position im eigenen Gegnerspielfeld auf "getroffen"
            self.spielfeld_gegner.koordinaten[y][x] = 3

            # Ziehe dem Gegner ein Leben ab
            gegner.leben = gegner.leben - 1
            
            return True

        # Ausgewaehlte Koordinate ist Wasser
        elif gegner.spielfeld_selbst.koordinaten[y][x] == 1:
            # Setze die Position im eigenen Gegnerspielfeld auf "Wasser"
            self.spielfeld_gegner.koordinaten[y][x] = 1
            
            return False


# # # # # # # # # # # # #
# Gegner (KI)
# # # # # # # # # # # # #

class Gegner(Spieler):

    def __init__(self):
        
        super().__init__()

    
    # Greife Spieler an
    def angriff(self, gegner):

        # Generiere Zielkoordinaten
        x = random.randint(0, 7)
        y = random.randint(0, 7)

        # Falls Koordinaten schon einmal beschossen wurden, generiere neue
        while ((self.spielfeld_gegner.koordinaten[y][x] == 3) or
                (self.spielfeld_gegner.koordinaten[y][x] == 1)):
            x = random.randint(0, 7)
            y = random.randint(0, 7)

        # Speichere, auf welche Position geschossen wurde
        self.spielfeld_gegner.letzter_treffer = (x, y)

        # Treffer, wenn Schiff an der Stelle im Koordinatensystem liegt
        if gegner.spielfeld_selbst.koordinaten[y][x] == 2:
            # Feld an der Stelle wird auf 'getroffen' gesetzt
            gegner.spielfeld_selbst.koordinaten[y][x] = 3

            # Speichert getroffenes schiff
            self.spielfeld_gegner.koordinaten[y][x] = 3

            # Leben wird abgezogen
            gegner.leben = gegner.leben - 1 
            
            return True

        # Wasser; kein Treffer
        elif gegner.spielfeld_selbst.koordinaten[y][x] == 1:  # gegner koordinaten gleich Wasser
            self.spielfeld_gegner.koordinaten[y][x] = 1  # speichert wasser position
            
            return False
            

    # Generiere eigene Schiffe
    def generate_ships(self, spielfeld):
        
        stop = False
        
        # ship leng 4
        while (stop != True):
            self.schiffe[0].x = random.randint(0, 7)
            self.schiffe[0].y = random.randint(0, 7)
            self.schiffe[0].laenge = 4
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[0], self.schiffe[0].x, self.schiffe[0].y, richtung)

        # ship leng 3 * 1
        stop = False
        while (stop != True):
            self.schiffe[1].x = random.randint(0, 7)
            self.schiffe[1].y = random.randint(0, 7)
            self.schiffe[1].laenge = 3
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[1], self.schiffe[1].x, self.schiffe[1].y, richtung)

        # ship_leng 3 * 2
        stop = False
        while (stop != True):
            self.schiffe[2].x = random.randint(0, 7)
            self.schiffe[2].y = random.randint(0, 7)
            self.schiffe[2].laenge = 3
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[2], self.schiffe[2].x, self.schiffe[2].y, richtung)

        # ship_leng 2 * 1
        stop = False
        while (stop != True):
            self.schiffe[3].x = random.randint(0, 7)
            self.schiffe[3].y = random.randint(0, 7)
            self.schiffe[3].laenge = 2
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[3], self.schiffe[3].x, self.schiffe[3].y, richtung)

        # ship_leng 2 * 2
        stop = False
        while (stop != True):
            self.schiffe[4].x = random.randint(0, 7)
            self.schiffe[4].y = random.randint(0, 7)
            self.schiffe[4].laenge = 2
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[4], self.schiffe[4].x, self.schiffe[4].y, richtung)

        # Schiff leng 1 *1
        stop = False
        while (stop != True):
            self.schiffe[5].x = random.randint(0, 7)
            self.schiffe[5].y = random.randint(0, 7)
            self.schiffe[5].laenge = 1
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[5], self.schiffe[5].x, self.schiffe[5].y, richtung)

        # Schiff leng 1 *2
        stop = False
        while (stop != True):
            self.schiffe[6].x = random.randint(0, 7)
            self.schiffe[6].y = random.randint(0, 7)
            self.schiffe[6].laenge = 1
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[6], self.schiffe[6].x, self.schiffe[6].y, richtung)

        # Schiff leng 1 *2
        stop = False
        while (stop != True):
            self.schiffe[7].x = random.randint(0, 7)
            self.schiffe[7].y = random.randint(0, 7)
            self.schiffe[7].laenge = 1
            richtung = random.randrange(0, 3, 2)
            stop = spielfeld.setze_schiff(self.schiffe[7], self.schiffe[7].x, self.schiffe[7].y, richtung)

# -*- coding: utf-8 -*-


"""
Konstanten
----------
Hier werden Konstanten (z.B. Farbwerte oder Codes fuer Buttons) gespeichert.
"""


# # # # # # # # # # # # #
# Terminal-Farben
# # # # # # # # # # # # #

# Rot
FB_TTY_ROT = 31
# Gruen
FB_TTY_GRU = 32
# Gelb
FB_TTY_GEL = 33
# Blau
FB_TTY_BLA = 34
# Violett
FB_TTY_VIO = 35
# Cyan
FB_TTY_CYA = 36
# Normal (idR Weiss)
FB_TTY_NOR = 0


# # # # # # # # # # # # #
# Launchpad-Farben
# # # # # # # # # # # # #

# Rot
FB_LPD_ROT = 3
# Gruen
FB_LPD_GRU = 48
# Gelb
FB_LPD_GEL = 51
# ...


# # # # # # # # # # # # #
# Buttons (Typen)
# # # # # # # # # # # # #

# Matrixknopf
BTN_MAT = 100
# Obere Leiste
BTN_KFL = 101
# Seitenleiste
BTN_STL = 102


# # # # # # # # # # # # #
# Buttons (Namen)
# # # # # # # # # # # # #

# Hoch
NAM_HCH = 200
# Runter
NAM_RNT = 201
# Links
NAM_LKS = 202
# Rechts
NAM_RTS = 203
# Session
NAM_SSN = 204
# User1
NAM_US1 = 205
# User2
NAM_US2 = 206
# Mixer
NAM_MIX = 207
# Generischer Koordinatenmatrixknopf (nur der Vollstaendigkeit halber)
NAM_KRD = 18
# Seitenleistenknopf
NAM_STL = 19

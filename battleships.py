#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
Copyright (C) 2019 Lukas Basedow, Lorenz Gillner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


"""
Hauptskript
-----------
Hier findet das eigentliche Spiel statt. Das Spiel kann durch das Kommando

    $ python3 battleships.py

oder direkt durch

    $ ./battleships.py

aufgerufen werden.
"""


import copy
import random
import sys
import os

from interface import Interface
from spiel import Spielfeld, Schiff, Spieler, Gegner
from konstanten import *
from grafiken import *


# # # # # # # # # # # # #
# Globale Variablen
# # # # # # # # # # # # #

# Debugging an oder aus
DEBUG = False

# Merke, ob Spieler oder Gegner dran ist
SPIELER_DRAN = False

# Maximale Summe des Spielfeldes eines Spielers
SUM_LIMIT = 81


# # # # # # # # # # # # #
# Vorbereitungs-Phase
# # # # # # # # # # # # #

def vorbereitung(interface):

    interface.info("Starte Vorbereitung...")

    # Mache LP bereit
    interface.ready()

    # Falls LP gefunden ...
    if interface.READY:
        # ... zeige eine Animation
        interface.start()

    interface.erfolg("Vorbereitung abgeschlossen.")


# # # # # # # # # # # # #
# Setzen-Phase
# # # # # # # # # # # # #

def phase_setzen(interface, button, spieler):

    # Starte mit dem Setzen, sobald Seitenleistenbutton losgelassen
    if (not button.status):
        # Speichere Auswahl
        auswahl = button.y

        # Schalte alle LEDs aus, damit neu gezeichnet werden kann
        interface.reset_mat()

        # Setze Richtung standardmaessig auf 0, also l-r
        richtung = 0

        # Hole Schiff aus dem Schiffstack; verringere y um 1, da
        # Seitenleistenbuttons bei 1 anfangen
        schiff = spieler.schiffe[auswahl-1]

        interface.info("Schiff {} gewaehlt".format(auswahl-1))

        # Warte so lange, bis der Spieler sein Schiff richtig gesetzt
        # hat. Das heißt: sobald der Spieler ein Schiff gewaehlt hat,
        # gibt es kein Zurueck mehr >:)
        while not schiff.gesetzt:
            interface.info("Schiff kann jetzt gesetzt werden.")

            interface.info("Aktuelle Richtung: {}".format(richtung))

            # Zeige alle bisher gesetzten Schiffe
            interface.zeige_schiffe_setzen(spieler)
            interface.zeige_schiffe_setzen(spieler)

            # Zeichne die Richtungsbuttons;
            interface.zeige_richtungsbuttons(richtung)

            # Zeichne Seitenleistenbuttons neu;
            # alle verfuegbaren dunkelgelb ausser zuletzt gedrueckter;
            for i in range(1, 9):
                if (spieler.schiffe[i-1].gesetzt):
                    interface.set_led(8, i, 1, 0)

                else:
                    if (i == auswahl):
                        interface.set_led(8, i, 3, 3)

                    else:
                        interface.set_led(8, i, 1, 1)

            # Schalte den Seitenleistenknopf an, der gerade gedrueckt
            # wurde; Spieler mit gutem Erinnerungsvermoegen wissen
            # dann, was fuer ein Schiff sie gerade setzen :o)
            interface.set_led(8, auswahl, 3, 3)

            # Warte auf Eingabe
            button = interface.get_button()

            # Switche je nach Art des gedrueckten Buttons;
            # 1. Kopfleistenbutton; Aendere Richtung ...
            if (button.typ == BTN_KFL):
                # ... aber natuerlich nur, wenn es einer der Richtungs-
                # buttons ist (also Namenswert kleiner als (session))
                if (button.name < NAM_SSN):
                    # Setze Richtung auf gewaehlten Wert
                    richtung = 203 - button.name

                    # Zeichne Richtungsbuttons neu und highlighte den
                    # Button mit der aktuellen Richtung.
                    interface.zeige_richtungsbuttons(richtung)

                    interface.info("Aktuelle Richtung: {}".format(richtung))

                elif (button.name == NAM_SSN):
                    # Zeige Hauptmenue bei Druecken des Session-Buttons
                    if (button.name == NAM_SSN) and (button.status):
                        # Zeige das Hauptmenue solange (session) gedrueckt
                        interface.hauptmenue()
                        interface.reset_mat()
                        continue

                # gedrueckter Button darf nicht gedrueckt werden
                else:
                    interface.zeige_grafik(GFK_FLR)

            # 2. Seitenleistenbutton; in diesem Moment nicht gestattet
            elif (button.typ == BTN_STL):
                interface.fehler("Button darf nicht gedrueckt werden")

                # Zeige rotes X
                interface.zeige_grafik(GFK_FLR)

            # 3. Button ist Matrixbutton; setze das Schiff
            elif (button.typ == BTN_MAT):
                # Wenn Schiff an erlaubte Position gesetzt wurde, ...
                if (spieler.spielfeld_selbst.setze_schiff(
                    schiff,
                    button.x,
                    button.y, # muessen wir machen wegen 9x9 vs 8x8
                    richtung
                )):
                    # ... zeige es kurz (x2 fuer Effekt), ...
                    interface.zeige_schiff(schiff, neu=True)
                    interface.zeige_schiff(schiff, neu=True)

                    # ... zeige das Schiff noch einen Augenblick
                    interface.warte(0.4)

                    # ... und setze seinen Status auf 'gesetzt'.
                    # -> Bricht hier die Schleife ab <-
                    schiff.gesetzt = True

                    interface.erfolg(
                        "Schiff erfolgreich gesetzt an {} {}".format(
                            schiff.x,
                            schiff.y
                        )
                    )

                    interface.zeige_grafik(GFK_EFG)

                # Bei fehlerhafter Position, zeige rotes Kreuz und
                # brich ab; starte Setzen neu
                else:
                    interface.zeige_grafik(GFK_FLR)


# # # # # # # # # # # # #
# Init-Phase
# # # # # # # # # # # # #

def phase_init(interface, spieler, gegner):

    # Gib Infos ueber den Spieler aus
    interface.info("Spielerdaten:\n{}".format(spieler))
    
    # Generiere gegnerische Schiffe
    gegner.generate_ships(gegner.spielfeld_selbst)

    # Gib Infos ueber den Gegner aus
    interface.info("Gegnerdaten:\n{}".format(gegner))
    
    interface.info("Starte Init-Phase ...")

    # Schalte Nutzer-Indikatoren ein
    interface.set_named_led(NAM_US1, 0, 3)
    interface.set_named_led(NAM_US2, 0, 1)

    # Schalte Session auf gelb, da er jetzt bedienbar ist
    interface.set_named_led(NAM_SSN, 1, 1)
    
    # 'Dauerschleife', solange Summe des Spielfeldes kleiner als SUM_LIMIT ist.
    # Da jedes Mal, wenn ein Schiff auf dem Spielfeld platziert wird, die
    # Summe der Koordinatenmatrix um Laenge des Schiffes erhoeht wird und
    # das Spielfeld des Spielers bereits von Anfang an mit 1en besetzt ist,
    # dauert die Phase des Schiffe-Setzens so lange, bis eine Summe von
    # SUM_LIMIT von der Koordinatenmatrix  des Spielers erreicht wurde.
    while sum(map(sum, spieler.spielfeld_selbst.koordinaten)) < SUM_LIMIT:
        # Zeige verfuegbare Schiffe
        interface.zeige_schiffe_init(spieler)
        
        # Schalte restliche Kopfleistenbuttons auf rot
        for name in range(NAM_HCH, NAM_MIX+1):
            if (name == NAM_US1) or (name == NAM_US2) or (name == NAM_SSN):
                pass

            else:
                interface.set_named_led(name, 1, 0)

        # Warte auf Eingabe
        button = interface.get_button()

        # Switche je nach Button-Typ;
        # Kopfleistenbutton
        if (button.typ == BTN_KFL):
            # Zeige Hauptmenue bei Druecken des Session-Buttons
            if (button.name == NAM_SSN) and (button.status):
                # Zeige das Hauptmenue solange (session) gedrueckt
                interface.hauptmenue()
                continue

            else:
                interface.zeige_grafik(GFK_FLR, 0.7)
                continue

        # Matrixbutton
        elif (button.typ == BTN_MAT):
            interface.zeige_grafik(GFK_WRN, 0.5)
            continue

        # Seitenleistenbutton
        elif (button.typ == BTN_STL):
            if (spieler.schiffe[button.y-1].gesetzt):
                interface.fehler("Schiff wurde bereits gesetzt.")
                interface.zeige_grafik(GFK_FLR)
                continue

            else:
                interface.set_led(8, button.y, 3, 3)
                
                # Da der Nutzer den Button auch wieder loslassen muss, warten
                # wir einfach, bis er das tut. Weil der losgelassene Button
                # dann auch der sein sollte, den er vorher gedrueckt hat,
                # koennen wir in den 'Setzen' Modus
                interface.get_button()

                phase_setzen(interface, button, spieler)

    interface.erfolg("Alle Schiffe gesetzt!")
    
    # Lass "Gegner Schiffe setzen"; ist ja eigentlich schon passiert, aber
    # das weiss der Spieler ja nicht ;)
    interface.reset_mat()

    # Schalte Nutzer-Indikatoren um
    interface.set_named_led(NAM_US1, 0, 1)
    interface.set_named_led(NAM_US2, 0, 3)

    # Faerbe alle anderen Kopfleistenbuttons rot, da nicht bedienbar
    for name in range(NAM_HCH, NAM_MIX+1):
        if (name == NAM_US1) or (name == NAM_US2):
            pass
        else:
            interface.set_named_led(name, 1, 0)

    # Animiere das Setzen; zeichnet an zufaelliger Position in zufaelliger
    # Farbe einen Punkt und laesst ihn wieder verschwinden
    for j in range(1, 9, 1):
        for y in range(0, j, 1):
            interface.set_led(8, y, 1, 0)

        for y in range(j, 9, 1):
            if (y == j):
                interface.set_led(8, y, 3, 3)

            else:
                interface.set_led(8, y, 1, 1)

        for i in range(3):
            x = random.randint(0, 7)
            y = random.randint(1, 8)

            interface.fade_led(x, y, x%3, y%2, 0.1, 0)

    interface.reset_all()

    interface.warte(0.7)

    # Gib Infos ueber den Spieler nach dem Schiffesetzen aus
    interface.info("Spielerdaten:\n{}".format(spieler))

    interface.info("Starte Spielphase...")


# # # # # # # # # # # # #
# Spieler am Zug
# # # # # # # # # # # # #

def zug_spieler(interface, spieler, gegner):

    global SPIELER_DRAN
    
    # Zeige Spielerleben
    interface.zeige_leben(spieler)

    # Schalte Spielerindikator auf "Spieler 1" ...
    interface.set_named_led(NAM_US1, 0, 3)

    # ... und mache Indikator fuer "Spieler 2" / Gegner leiser
    interface.set_named_led(NAM_US2, 0, 1)

    # Schalte (mixer) auf gelb, weil er jetzt Bedienelement ist
    interface.set_named_led(NAM_MIX, 1, 1)

    # Schalte Session auf gelb, da er jetzt bedienbar ist
    interface.set_named_led(NAM_SSN, 1, 1)

    # Zeige eine grosse 1
    if (not SPIELER_DRAN):
        interface.zeige_grafik(GFK_US1, 1.3)
        SPIELER_DRAN = True

    while SPIELER_DRAN:
        # Zeige das gegnerische Spielfeld fuer die Spielphase
        interface.zeige_spielfeld(spieler.spielfeld_gegner)
        
        # Warte auf naechstes Button-Event
        button = interface.get_button()

        # Switche je nach Art des Buttons
        # 1. Kopfleistenbutton
        if (button.typ == BTN_KFL):
            # Button ist (mixer)
            if (button.name == NAM_MIX) and (button.status):
                # Zeige eine Karte mit den eigenen Schiffen
                interface.karte(spieler)
                continue

            # Button ist (session)
            elif (button.name == NAM_SSN):
                # Zeige das Hauptmenue
                interface.hauptmenue()
                continue

            else:
                continue


        # 2. Seitenleistenbutton; zeigt in aktueller Phase Leben in Textform
        elif (button.typ == BTN_STL):
            interface.zeige_leben_text(spieler)

            # Ueberspringe den Rest der Schleife
            continue

        elif (button.typ == BTN_MAT):
            # Spieler greift Gegner an
            x_ziel = button.x
            y_ziel = button.y - 1

            treffer = spieler.angriff(x_ziel, y_ziel, gegner)

            # Ziel wurde schon einmal beschossen
            if (treffer is None):
                interface.zeige_grafik(GFK_FLR)

                continue

            # Wir haben etwas getroffen
            if (treffer):
                interface.treffer(x_ziel, y_ziel)

            # Wir haben ins Wasser geschossen
            elif (not treffer):
                pass

            # Zeige Spielfeld noch kurz an
            interface.zeige_spielfeld(spieler.spielfeld_gegner)

            interface.warte(1.5)

            SPIELER_DRAN = False


# # # # # # # # # # # # #
# Gegner ist am Zug
# # # # # # # # # # # # #

def zug_gegner(interface, gegner, spieler):
    
    if (gegner.leben > 0):
        interface.info("Gegner ist am Zug")

        # Schalte (session) und (mixer) rot, da nicht bedienbar atm
        interface.set_named_led(NAM_SSN, 1, 0)
        interface.set_named_led(NAM_MIX, 1, 0)

        # Zeige an den Nutzerindikatoren, dass Gegner dran ist
        interface.set_named_led(NAM_US1, 0, 1)
        interface.set_named_led(NAM_US2, 0, 3)

        # Zeige gegnerische Lebenspunkte
        interface.zeige_leben(gegner)

        # Zeige eine grosse 2
        interface.zeige_grafik(GFK_US2, 1.3)

        # Zeige gegnerisches Spielfeld des Bots, also das, auf dem
        # Spielerschiffe aufgedeckt werden muessen
        interface.zeige_spielfeld(gegner.spielfeld_gegner)

        interface.warte(1.2)

        # Kopiere altes Spielfeld wegen der Animation fuer Zielsuche
        spielfeld_alt = copy.deepcopy(gegner.spielfeld_gegner)

        # Greife den Spieler an und speichere das Ergebnis
        treffer = gegner.angriff(spieler)

        # Hole Koordinaten vom angegriffenen Punk, weil Koordinaten dafuer
        # in der Funktion selbst berechnet werden
        x_ziel = gegner.spielfeld_gegner.letzter_treffer[0]
        y_ziel = gegner.spielfeld_gegner.letzter_treffer[1]

        interface.info("Gegner hat ({};{}) gewaehlt".format(x_ziel, y_ziel))

        interface.info("Schiff wurde{}getroffen".format(
            " " if treffer else " nicht ")
        )

        # Animiere die Zielsuche
        interface.zielsuche(x_ziel, y_ziel, spielfeld_alt)

        # Wenn getroffen, dann zeige Trefferanimation
        if (treffer is True):
            interface.treffer(x_ziel, y_ziel)
            interface.warte(0.1)

        # Sonst: zeige das Fadenkreuz noch etwas laenger an den
        # Zielkoordinaten
        elif (not treffer):
            interface.zeige_fadenkreuz(x_ziel, y_ziel, 2, 1)
            interface.warte(0.3)

        interface.zeige_spielfeld(gegner.spielfeld_gegner)

        interface.warte(1.2)


# # # # # # # # # # # # #
# Hauptfunktion
# # # # # # # # # # # # #

def main(interface):

    # Lass Spiel so lange laufen, bis Spieler den Ausschalter drueckt (Menue)
    while True:
        # Spielvorbereitung
        vorbereitung(interface)

        # Erzeuge unsere beiden Spielpartner
        spieler = Spieler()
        gegner = Gegner()

        # Iniziiere Spielfelder und lass Spieler seine Schiffe setzen
        phase_init(interface, spieler, gegner)

        # # # # # # # # # # # # #
        # Spielphase
        # # # # # # # # # # # # #
        
        while (spieler.leben > 0) and (gegner.leben > 0):
            
            # Lass den Spieler eine Position beschiessen
            zug_spieler(interface, spieler, gegner)
            
            # Spieler hat gewonnen;
            # zeige 'V' fuer Sieg und brich Schleife ab
            if (gegner.leben == 0):
                interface.reset_all()
                interface.zeige_grafik(GFK_GWN, 2.0)
                break

            # Lass den Gegner den Spieler angreifen
            zug_gegner(interface, gegner, spieler)

            # Gegner hat gewonnen
            # Zeige 'L' fuer Niederlage und brich Schleife ab
            if (spieler.leben == 0):
                interface.reset_all()
                interface.zeige_grafik(GFK_VLN, 2.0)
                break

        # Mache alle Lichter aus
        interface.reset_all()

        interface.READY = False


if __name__ == "__main__":

    # Stelle Verbindung zum LP her
    interface = Interface(debug=DEBUG)

    # Starte Hauptskript in Endlosschleife
    main(interface)

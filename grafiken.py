# -*- coding: utf-8 -*-


"""
Grafiken / Symbole
------------------
Hier liegen alle "Bilder" die vom LP gezeigt werden sollen; also 8x8 Grafiken,
die immer gleich sind und deshalb nicht bei jedem Aufruf neu berechnet werden
muessen, sondern direkt als Farbsequenz an das LP geschickt werden koennen.
Zur einfacheren Lesbarkeit werden die Listen in zwei Dimensionen dargestellt
(sind aber immer noch LISTEN, keine LISTEN AUS LISTEN!).
Bei Grafiken, die ueber die Spielfeldmatrix hinaus gehen sollen, immer daran
denken: nach der Matrix kommt erst die Seitenleite, dann die Kopfleiste.
"""


from konstanten import *


# Grosses rotes X, das bei Fehlern gezeigt wird
GFK_FLR = [
    FB_LPD_ROT, 0, 0, 0, 0, 0, 0, FB_LPD_ROT,\
    0, FB_LPD_ROT, 0, 0, 0, 0, FB_LPD_ROT, 0,\
    0, 0, FB_LPD_ROT, 0, 0, FB_LPD_ROT, 0, 0,\
    0, 0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0,\
    0, 0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0,\
    0, 0, FB_LPD_ROT, 0, 0, FB_LPD_ROT, 0, 0,\
    0, FB_LPD_ROT, 0, 0, 0, 0, FB_LPD_ROT, 0,\
    FB_LPD_ROT, 0, 0, 0, 0, 0, 0, FB_LPD_ROT,\
]


# Gruener Haken, der bei Erfolg gezeigt wird
GFK_EFG = [
    0, 0, 0, 0, 0, 0, 0, 0,\
    0, 0, 0, 0, 0, 0, FB_LPD_GRU, FB_LPD_GRU,\
    0, 0, 0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0,\
    0, 0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0,\
    FB_LPD_GRU, FB_LPD_GRU, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0, 0,\
    0, 0, FB_LPD_GRU, 0, 0, 0, 0, 0,\
    0, 0, 0, 0, 0, 0, 0, 0
]


# Zahl '1'
GFK_US1 = [
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0
]


# Zahl '2'
GFK_US2 = [
    0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0,\
    0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0,\
    0, FB_LPD_GRU, 0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0,\
    0, 0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0,\
    0, 0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0, 0,\
    0, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU, FB_LPD_GRU,\
    FB_LPD_GRU, FB_LPD_GRU, 0
]


# Bei gewonnenem Spiel: 'V' fuer 'Victory'
GFK_GWN = [
    0, FB_LPD_GRU, 0, 0, 0, 0, FB_LPD_GRU, 0,\
    0, FB_LPD_GRU, 0, 0, 0, 0, FB_LPD_GRU, 0,\
    0, FB_LPD_GRU, 0, 0, 0, 0, FB_LPD_GRU, 0,\
    0, 0, FB_LPD_GRU, 0, 0, FB_LPD_GRU, 0, 0,\
    0, 0, FB_LPD_GRU, 0, 0, FB_LPD_GRU, 0, 0,\
    0, 0, FB_LPD_GRU, 0, 0, FB_LPD_GRU, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0,\
    0, 0, 0, FB_LPD_GRU, FB_LPD_GRU, 0, 0, 0
]


# Bei Niederlage: 'L' fuer 'Loss'
GFK_VLN = [
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, 0, 0, 0, 0,\
    0, 0, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT,\
    0, 0, 0, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT, FB_LPD_ROT
]


# Ausrufezeichen
GFK_WRN = [
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, 0, 0, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0,\
    0, 0, 0, FB_LPD_GEL, FB_LPD_GEL, 0, 0, 0
]

# -*- coding: utf-8 -*-


"""
Ein- u. Ausgabe
---------------
Das Interface dient dazu, die Ein- und Ausgabe ueber den Launchpad (LP) MIDI-
Controller zu verarbeiten.
"""


import threading
import time
import sys
import os
from launchpad_py import Launchpad

from konstanten import *
from spiel import Spielfeld, Spieler, Schiff
from bresenham import bresenham


# Button Objekte, die returnt werden koennen
class Button():

    def __init__(self, x, y, status=False, typ=None, name=None, r=0, g=0):

        self.x = x
        self.y = y
        self.status = status
        self.typ = typ
        self.name = name
        self.r = r
        self.g = g


# Blinker laesst einen Knopf heller und dunkler werden, bis exit_status True
class Blinker(threading.Thread):

    def __init__(self, interface, threadID, name, button, r, g, t):

        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.interface = interface
        self.button = button
        self.r = r
        self.g = g
        self.t = t

    def run(self):

        self.interface.blinke(self.button, self.r, self.g, self.t)


# Klasse des Objekts, welches LP-IO verwaltet
class Interface():

    # Debug variable schaltet den Debug-Modus ein oder aus
    DEBUG = None

    # Wird erstmal gebraucht wegen des "Restart-Bugs" ... :(
    READY = None

    # Status-Flag fuer Blinker
    EXIT_STATUS = False

    # Puffer zum Zwischenspeichern der LED-Werte; wird hier 'szene' genannt,
    # weil es ja prinzipiell eine Szene des Spiels speichert
    szene = []

    # Erzeuge neues Interface; modus fuer Debug-Informationen
    def __init__(self, debug=False):

        # Setze Debug-Modus je nach Wert von modus
        self.DEBUG = debug

        # Launchpad-Objekt
        self.lp = Launchpad()

        # Matrix von Buttons, damit nicht immer ein neues Button-Objekt erzeugt
        # werden muss, wenn man auf einen LP-Button zugreift.
        self.buttons = [[Button(x, y) for x in range(9)] for y in range(9)]

        # Buttons mit richtigen Namen und Typen versehen
        # Zuerst die Kopfleistenbuttons ...
        for i in range(len(self.buttons[0])):
            self.buttons[0][i].typ = BTN_KFL
            self.buttons[0][i].name = (200 + i)

        # ... dann die Seitenleistenself.buttons ...
        for j in range(1, 9):
            self.buttons[j][8].typ = BTN_STL
            self.buttons[j][8].name = NAM_STL

        # ... und zum Schluss die Matrixself.buttons
        for y in range(1, 9):
            for x in range(0, 8):
                self.buttons[y][x].typ = BTN_MAT
                self.buttons[y][x].name = NAM_KRD


        # Versuche Verbindung zum LP aufzubauen, wenn es Mk1 ist
        if self.lp.Check(0, "Launchpad"):
            # Teste, ob Verbindung zum LP hergestellt werden kann
            if (self.lp.Open()):
                # Loesche LP-Puffer
                self.lp.ButtonFlush()

                # Mache alle LEDs aus
                self.lp.Reset()

                # Melde, dass LP gefunden wurde
                self.erfolg("Verbindung zum Launchpad hergestellt.")

                # Setze READY auf False, weil zwar Verbindung hergestellt
                # wurde, LP aber unter Umstaenden noch nicht auf den ersten
                # Knopfdruck reagiert (bekannter Bug, meist nach Neustart)
                self.READY = False

            else:
                # Gib eine Meldung aus, wenn kein Launchpad gefunden wurde
                raise Exception("Kein Launchpad gefunden!")

        else:
            # Fehlermeldung bei falschem LP Modell
            raise Exception("Diese Launchpad-Version wird nicht unterstuetzt!")


    # Pruefe, ob Buttons registriert werden;
    # erst wenn Button-Events wirklich aufgenommen werden, darf READY True sein
    def ready(self):

        if (not self.READY):
            blinker = Blinker(self, 1, "Blinker", NAM_MIX, 1, 1, 0.3)

            self.info("Druecke den (mixer) Button, um LP aufzuwecken")

            # Starte Thread, der den Mixer-Button blinken laesst
            blinker.start()

            # Etwas hacky, weil es eigentlich egal ist, welcher
            # Kopfleistenbutton gedrueckt wurde, aber wenn man genaue Knoepfe
            # abfragt, spinnt er rum
            while not self.lp.ButtonChanged():
                pass

            self.erfolg("OK!")

            # Beende Blinker-Thread
            self.EXIT_STATUS = True
            blinker.join()

            # Setze READY-Flag endlich auf True
            self.READY = True

        else:
            self.info("LP ist bereits bereit.")


    # Hole einen Button und seine Informationen (Nutzereingabe)
    def get_button(self):

        # ... natuerlich nur, wenn LP bereit ist ...
        if (self.READY):
            # leere LP-Button-Puffer
            self.lp.ButtonFlush()

            # Warte, bis Button gedrueckt wurde
            while True:
                if (self.lp.ButtonChanged()):
                    # Hole Informationen aus Button-Event
                    event = self.lp.ButtonStateXY()

                    # Setze Gedrueckt-Status des Elements im Button-Speicher
                    self.buttons[event[1]][event[0]].status = event[2]

                    # Hole Button aus Buttonmatrix an Position (x;y)
                    button = self.buttons[event[1]][event[0]]

                    self.lp.ButtonFlush()

                    # Gib im Debug-Modus den Button aus
                    self.info("Button {}: {} {} {} {}".format(
                        "gedrueckt" if button.status else "losgelassen",
                        button.x,
                        button.y,
                        button.typ,
                        button.name
                    ))

                    return button

        else:
            # Melde, dass LP noch nicht bereit ist
            self.fehler("LP ist noch nicht bereit!")

            return None


    # "Huebsche" Startanimation
    def start(self):

        # Linie von oben und unten zu Mitte
        for y in range(0, 4, 1):
            for x in range(8):
                self.set_led(x, y+1, 3, 3)
                self.set_led(x, y+1, 3, 3)
                self.set_led(x, 8-y, 3, 3)
                self.set_led(x, 8-y, 3, 3)

            self.warte(0.1)

            for x in range(8):
                self.set_led(x, y+1, 0, 0)
                self.set_led(x, y+1, 0, 0)
                self.set_led(x, 8-y, 0, 0)
                self.set_led(x, 8-y, 0, 0)

            self.warte(0.1)

        # Linie von Mitte nach links und rechts
        for x in range(0, 3, 1):
            for y in range(1, 9, 1):
                self.set_led(3-x, y, 3, 3)
                self.set_led(3-x, y, 3, 3)
                self.set_led(4+x, y, 3, 3)
                self.set_led(4+x, y, 3, 3)

            self.warte(0.1)

            for y in range(1, 9, 1):
                self.set_led(3-x, y, 0, 0)
                self.set_led(3-x, y, 0, 0)
                self.set_led(4+x, y, 0, 0)
                self.set_led(4+x, y, 0, 0)

            self.warte(0.1)


    # Eigene Schiffe
    def karte(self, spieler):

        # Mixer Button vollgelb, da er aktiv ist
        self.set_named_led(NAM_MIX, 3, 3)

        # Zeige das eigene Spielfeld (falls man vergessen hat, wo man
        # seine Schiffe geparkt hat :^) )
        self.zeige_spielfeld(spieler.spielfeld_selbst)

        # Warte, bis (mixer) losgelassen wurde
        while (self.buttons[0][7].status):
            button = self.get_button()
            
            if (button.name == NAM_MIX) and (not button.status):
                break

            else:
                pass


    # Hauptmenue
    def hauptmenue(self):
        
        # Session Button vollgelb, da er aktiv ist
        self.set_named_led(NAM_SSN, 3, 3)

        # Matrix leeren
        self.reset_mat()

        # Zeichne 2 2x2 Button-Felder ("grosse Buttons"),
        # eines fuer Neustart ...
        self.set_led(1, 4, 3, 3)
        self.set_led(1, 5, 3, 3)
        self.set_led(2, 4, 3, 3)
        self.set_led(2, 5, 3, 3)

        # ... und eines, um das Spiel zu beenden
        self.set_led(5, 4, 3, 0)
        self.set_led(5, 5, 3, 0)
        self.set_led(6, 4, 3, 0)
        self.set_led(6, 5, 3, 0)

        # Warte auf Eingabe; akzeptiere nur Buttons aus den Button-Feldern
        # (oder einen losgelassenen Session-Button)
        while (self.buttons[0][4].status):
            # Warte auf Eingabe
            auswahl = self.get_button()

            if (auswahl.name == NAM_SSN) and (not auswahl.status):
                self.set_named_led(NAM_SSN, 1, 1)
                self.reset_mat()
                break

            # Gelber Restartbutton
            elif ((0 < auswahl.x < 3) and (6 > auswahl.y > 3)):
                self.info("Starte Spiel neu")

                self.READY = False
                
                # Zuerst Interface schließen
                self.close()

                # Spiel neu starten
                command = "./battleships.py"
                os.system("python3 {}".format(command))

            # Roter Beendenbutton
            elif ((4 < auswahl.x < 7) and (6 > auswahl.y > 3)):
                # Gib die Debug-Nachricht aus, dass Spiel beendet wird
                self.info("Beende Spiel")

                # Wichtig: schliesse Verbindung zum LP
                self.close()

                # Beende Programm
                sys.exit()

            else:
                pass


    # Zeige ein einzelnes Schiff
    def zeige_schiff(self, schiff, neu=False):

        # Faktor zur Bestimmung, in welche Richtung gezeichnet wird
        richtung = 1 if (schiff.richtung % 2 == 0) else (-1)

        # Schiff liegt horizontal (0: l-r; 1: r-l)
        if (schiff.richtung < 2):
            for x in range(schiff.laenge):
                # Neue Schiffe werden beim Setzen mit einer kleinen Animation
                # vom Rest der Schiffe abgehoben
                if (neu):
                    self.fade_led(schiff.x+richtung*x, schiff.y, 1, 1,
                        0.2/schiff.laenge) # je laenger das Schiff, desto
                                           # kuerzer die Animation zwischendrin

                else:
                    self.set_led(schiff.x+richtung*x, schiff.y, 3, 3)

        # Schiff liegt vertikal (2: o-u; 3: u-o)
        elif (schiff.richtung >= 2):
            for y in range(schiff.laenge):
                if (neu):
                    self.fade_led(schiff.x, schiff.y+richtung*y, 1, 1,
                        0.2/schiff.laenge)

                else:
                    self.set_led(schiff.x, schiff.y+richtung*y, 3, 3)

        elif (schiff.richtung > 3):
            self.fehler("Keine gueltige Richtung!")


    # Fuer Init-Phase: zeige alle noch setzbaren Schiffe des Spielers
    def zeige_schiffe_init(self, spieler):

        # Hole Spielerschiffe
        schiffe = spieler.schiffe

        # Wir koennen hier die Range vorschreiben, weil sich die nie aendert;
        # Beginn bei 1, weil BTN_STL bei 1 beginnen und bis 8 gehen
        for y in range(1, 9):
            # Falls Schiff schon gesetzt wurde
            if (schiffe[y-1].gesetzt):
                # Male leere Stelle
                self.set_led(8, y, 1, 0)

            # Ansonsten:
            else:
                # Schalte Seitenleistenbutton an
                self.set_led(8, y, 1, 1)

                # Zeichne das Schiff
                for x in range(schiffe[y-1].laenge):
                    self.set_led(7-x, y, 3, 3)


    # Zeige Schiffe an, waehrend ein neues Schiff gesetzt werden kann;
    # spieler ist i.d.R. der echte Spieler und auswahl ist der Button an der
    # Seitenleiste, fuer die Auswahl des Schiffes (Button.y)
    def zeige_schiffe_setzen(self, spieler):
        
        # Hole Spielerschiffe
        schiffe = spieler.schiffe

        for schiff in schiffe:
            # Zeichne Schiff in Spielfeld, wenn es schon gesetzt wurde
            if (schiff.gesetzt):
                # Zeichne das Schiff
                self.zeige_schiff(schiff)


    # Zeige die Richtungsbuttons
    def zeige_richtungsbuttons(self, richtung=0):

        for i in range(NAM_HCH, NAM_SSN):
            if ((203 - i) == richtung):
                self.set_named_led(i, 3, 3)

            else:
                self.set_named_led(i, 1, 1)


    # Zeige die Lebenspunkte des Spielers spieler an der Seitenleiste
    # (abgebildet auf 8 Buttons)
    def zeige_leben(self, spieler):

        leben = (spieler.leben // 2)

        if (spieler.leben > 3):
            rot = (3 - leben // 2)
            gruen = (0 + leben // 2)

            for y in range(1, 9):
                if ((9 - y) > leben):
                    self.set_led(8, y, 0, 0)

                else:
                    self.set_led(8, y, rot, gruen)

        elif (spieler.leben <= 3):
            for y in range(1, 8):
                self.set_led(8, y, 0, 0)

            self.set_led(8, 8, spieler.leben, 0)

        elif (spieler.leben == 0):
            for y in range(1, 9):
                self.set_led(8, y, 0, 0)


    # Zeige die Leben eines Spielers in Textform
    def zeige_leben_text(self, spieler):

        leben = spieler.leben

        rot = (3 - leben // 2)
        gruen = (0 + leben // 2)

        if (spieler.leben != 0):
            self.lp.LedCtrlString(str(leben), rot, gruen, -1, 10)


    # Zeige das Spielfeld von Spieler 'spieler'
    def zeige_spielfeld(self, spielfeld):

        spielfeld = spielfeld.koordinaten

        for x in range(8):
            for y in range(8):
                # Nichts
                if (spielfeld[y][x] == 0):
                    # Wir muessen hier explizit alle LEDs ausschalten, weil
                    # man sonst ja nichts ueberschreiben kann (und Reset()
                    # schaltet leider ALLE LEDs aus)
                    self.set_led(x, y+1, 0, 0)

                # Wasser ist hellgruen
                elif (spielfeld[y][x] == 1):
                    self.set_led(x, y+1, 0, 1)

                # Heiles Boot ist gelb
                elif (spielfeld[y][x] == 2):
                    self.set_led(x, y+1, 3, 3)

                # Getroffenes Boot ist rot
                elif (spielfeld[y][x] == 3):
                    self.set_led(x, y+1, 3, 0)

                # WTF
                else:
                    self.fehler("Wert an ({};{}) ist ungueltig!".format(x, y))


    # Zeichne ein kleines Kreuz
    def zeige_fadenkreuz(self, x, y, r, g):

        # Zeichne eine senkrechte Linie von gedruecktem Button -1 bis +1
        for y_neu in range(y, y+3):
            # Setze LED nur dann, wenn y-Koordinate zwischen 1 und 8 liegt
            if (y_neu > 0) and (y_neu < 9):
                self.set_led(x, y_neu, r, g)

            # Wenn wir in der Mitte der Linie angekommen sind, setzen wir je
            # einen Punkt links und rechts, aber nur, wenn die jeweilige
            # Koordinate nicht ueber die Raender geht
            if (y_neu == (y+1)):
                if (x > 0):
                    self.set_led(x-1, y_neu, r, g)
                if (x < 7):
                    self.set_led(x+1, y_neu, r, g)


    # Trefferanimation
    def treffer(self, x, y):

        self.zeige_fadenkreuz(x, y, 3, 0)
        self.warte(0.1)
        self.zeige_fadenkreuz(x, y, 3, 1)
        self.warte(0.1)
        self.zeige_fadenkreuz(x, y, 3, 3)
        self.warte(0.4)


    # Zielsucheanimation
    def zielsuche(self, x_ziel, y_ziel, spielfeld):

        # Ermittle Startkoordinaten; nimm immer die am weitesten entfernte Ecke
        x_start = 0 if (x_ziel > 3) else 7
        y_start = 0 if (y_ziel > 3) else 7

        # Berechne die Koordinaten, die passiert werden muessen, um ans Ziel
        # zu gelangen; Danke an Petr Viktorin fuer den Bresenham!
        steps = list(bresenham(x_start, y_start, x_ziel, y_ziel))
        
        # Zeichne das Fadenkreuz an jedem Punkt der Gerade zum Ziel
        for step in steps:
            self.zeige_spielfeld(spielfeld)
            # Zeichne das Fadenkreuz ...
            self.zeige_fadenkreuz(step[0], step[1], 2, 1)

            # ... warte kurz ...
            self.warte(0.4)

        # ... und lass Fadenkreuz wieder verschwinden
        self.zeige_spielfeld(spielfeld)


    # Zeige ein Symbol auf dem LP an; Symbol muss eine Sequenz von Farben sein,
    # die an das LP gesendet werden
    def zeige_grafik(self, grafik, t=1.2):

        # Sende Sequenz von Farben an LP
        self.lp.LedCtrlRawRapid(grafik)

        # Warte t Sekunden
        self.warte(t)

        # Mach wieder alle Lichter aus
        self.reset_mat()


    # Schalte eine LED ein
    def set_led(self, x, y, r, g):

        # Schalte den Button an ...
        self.lp.LedCtrlXY(x, y, r, g)

        # ... und speichere seine Farbwerte
        self.buttons[y][x].r = r
        self.buttons[y][x].g = g


    # Schalte eine LED mit Namen (siehe typen.py) ein
    def set_named_led(self, name, r, g):

        # Schalte den Button an ...
        self.lp.LedCtrlRaw(name, r, g)

        x = name - 200

        self.buttons[0][x].r = r
        self.buttons[0][x].g = g


    # Einblenden ueber Zeitraum von 't' Sekunden;
    # -1: ausblenden, 0: ein- und ausblenden, 1: nur einblenden;
    # 'r' und 'g' koennen jeweils nur 0 xoder 1 sein
    def fade_led(self, x, y, r, g, t=0.1, modus=1):

        # Berechne die Zeit, die zwischen den Helligkeitsabstufungen gewartet
        # werden soll
        sekunden = t / 3

        if (r > 0) or (g > 0):
            if (modus == (-1)):
                for i in range(3, -1, -1):
                    # Schalte LED auf Farbe ...
                    self.set_led(x, y, r*i, g*i)

                    # Warte kurz
                    self.warte(sekunden)

            elif (modus == 1):
                for i in range(0, 4):
                    # Schalte LED ein
                    self.set_led(x, y, r*i, g*i)

                    # Warte kurz
                    self.warte(sekunden)

            elif (modus == 0):
                self.fade_led(x, y, r, g, t/2, modus=1)
                self.fade_led(x, y, r, g, t/2, modus=-1)


    # Einblenden einer benannten LED ueber Zeitraum von 't' Sekunden
    def fade_named_led(self, name, r, g, t=0.1, modus=1):

        # yea boi
        sekunden = t / 3

        if (r > 0) or (g > 0):
            if (modus == (-1)):
                for i in range(3, -1, -1):
                    # Schalte LED auf Farbe ...
                    self.set_named_led(name, r*i, g*i)

                    # Warte kurz
                    self.warte(sekunden)

            elif (modus == 1):
                for i in range(0, 4):
                    # Schalte LED ein
                    self.set_named_led(name, r*i, g*i)

                    # Warte kurz
                    self.warte(sekunden)

            elif (modus == 0):
                self.fade_named_led(name, r, g, t/2, modus=1)
                self.fade_named_led(name, r, g, t/2, modus=-1)


    # Lass einen Button (Raw) sachte blinken
    def blinke(self, button, r, g, t):

        self.EXIT_STATUS = False

        while not self.EXIT_STATUS:
            self.fade_named_led(button, r, g, t, 0)

        self.set_named_led(button, 0, 0)


    # Speichere alle aktuellen LED-Werte im Szenenspeicher, damit bei schnellen
    # Szenenwechseln nicht alle Farben und Positionen neu berechnet werden
    # muessen, sondern direkt als Raw-Werte ans LP gesendet werden koennen
    def hole_szene(self):

        # Leere den Szenenbuffer, falls er nicht leer ist
        if (len(self.szene) != 0):
            self.leere_szene()

        # Gehe jeden Button im Speicher durch und speichere seinen Farbwert
        # im LP-spezifischen Format im Szenenspeicher;
        # erst die Matrixbuttons ...
        for y in range(1, 9):
            for x in range(0, 8):
                self.szene.append(
                    self.lp.LedGetColor(
                        self.buttons[y][x].r,
                        self.buttons[y][x].g
                    )
                )

        # dann die Seitenleistenbuttons ...
        for y in range (1, 9):
            self.szene.append(
                self.lp.LedGetColor(
                    self.buttons[y][8].r,
                    self.buttons[y][8].g
                )
            )

        # ... und zum Schluss die Kopfleistenbuttons
        for x in range(0, 8):
            self.szene.append(
                self.lp.LedGetColor(
                    self.buttons[0][x].r,
                    self.buttons[0][x].g
                )
            )

        self.info("{}".format(self.szene))


    # "Rekonstruiere" eine gespeicherte Szene
    def zeige_szene(self):

        # Sende eine Liste von Farbwerten an das LP
        self.lp.LedCtrlRawRapid(self.szene)


    # Loesche Szenenspeicherinhalt
    def leere_szene(self):

        self.szene = []


    # Gib Text ueber LP aus (eigentlich nur Wrapper, tbqh)
    def zeige_text(self, text, r, g, richtung=(-1)):

        # Richtung: -1 r-l, 0 nichts, 1 l-r
        self.lp.LedCtrlString(text, r, g, richtung, 30)


    # Setze alle Buttons zurueck
    def reset_all(self):

        self.lp.Reset()

        for y in range(9):
            for x in range(9):
                self.buttons[y][x].r = 0
                self.buttons[y][x].g = 0
                self.buttons[y][x].status = False

        self.lp.ButtonFlush()


    # Setze alle Matrixbuttons zurueck
    def reset_mat(self):

        for y in range(1, 9):
            for x in range(8):
                self.set_led(x, y, 0, 0)
                self.buttons[y][x].r = 0
                self.buttons[y][x].g = 0
                self.buttons[y][x].status = False

    # Schließe Verbindung zum Launchpad
    def close(self):

        self.info("Schließe Verbindung zum Launchpad...")

        try:
            self.lp.Reset()
            self.lp.ButtonFlush()
            self.lp.Close()
            self.erfolg("Verbindung geschlossen.")

        except:
            self.fehler("Konnte Verbindung nicht schliessen.")


    # Wrapper fuer time.sleep()
    def warte(self, t):

        time.sleep(t)


    # Terminalmeldungen; nur fuer Debugging
    # Ausgabe von Info-Meldungen
    def info(self, meldung):

        self.melde(" Info ", FB_TTY_GEL, meldung)


    # Ausgabe positiver Meldungen
    def erfolg(self, meldung):

        self.melde("Erfolg", FB_TTY_GRU, meldung)


    # Ausgabe von Fehlermeldungen
    def fehler(self, meldung):

        self.melde("Fehler", FB_TTY_ROT, meldung)


    # Gib eine Meldung im Terminal aus; nur im Debug-Modus verfuegbar;
    # Meldungen haben einen farbigen Marker in folgender Form:
    #   [Kategorie]    Meldungstext
    # Der Marker dient dazu, schneller zu erkennen, um was fuer eine Art
    # Meldung es sich handelt.
    def melde(self, kategorie, farbe, meldung):

        if (self.DEBUG):
            # Faerbe den Marker der Nachricht je nach 'kategorie'
            print("[\x1b[{}m{}\x1b[{}m]\t{}".expandtabs(4).format(
                farbe, kategorie, FB_TTY_NOR, str(meldung)))

